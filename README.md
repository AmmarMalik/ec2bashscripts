# EC2 bash/User data scripts

A list of different Bash Scripts to use as bash scripts to spin up EC2 instances

## Getting started

These scripts are a sample or usually needed scripts to be used while spinning up an EC2 instance in AWS

## How to use

# Steps to use
- [ ] download shell script using wget
- [ ] Run downloaded .sh file. Remember it is not a zip file, it is a simple file

Sample code
```
#!/bin/bash
curl https://gitlab.com/api/v4/projects/35642466/repository/files/simpleWebServer.sh/raw?ref=master>simpleWebServer.sh
bash simpleWebServer.sh
```

Just copy/paste the code above to the EC2. Replace file name with the script name you want to download in wget and bash

## Bash Files explained

- Simple Web Server
This is a simple web server. It will download apache server, install it and then run it.

- WebServerWithPhpMySql
This script downloads Apache server, php and mySql and installs it respectively
It will run the apache server after installation
