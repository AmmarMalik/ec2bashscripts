#!/bin/bash
# Install Apache Web Server and PHP
yum install -y httpd mysql php
# Turn on web server
chkconfig httpd on
service httpd start
